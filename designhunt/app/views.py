"""

All basic views of features of restaurant other than its traits

"""

from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.db import connection
from django.core import serializers

from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

import json

from app.serializers import *
from .serializers import UserSerializer



class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
        
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class CompanyViewSet(viewsets.ViewSet):
    """
    A viewset that provides the standard actions
    """
    permission_classes = (IsAuthenticated,)
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    def list(self, request):
        if(request.query_params):
            queryset = Company.objects.filter(name__contains=request.query_params['search'])
        else:
            queryset = Company.objects.all()
        serializer = CompanySerializer(queryset, many=True,
                                       context={'request': request})
        
                                       
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = CompanyCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(Company, pk=pk)
        serializer = CompanyRetreiveSerialzier(instance,
                                       context={'request': request,'pk':pk})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        instance = get_object_or_404(Company, pk=pk)
        serializer = CompanyCreateSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        instance = get_object_or_404(Company, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class EmailViewSet(viewsets.ViewSet):
    """
    A viewset that provides the standard actions
    """
    permission_classes = (IsAuthenticated,)
    queryset = Email.objects.all()
    serializer_class = EmailSerializer

    def get_queryset(self):
        self.companies = Track.objects.filter(
            user=self.request.user).distinct().values_list('company')
        queryset = Email.objects.filter(company__in=self.companies)
        return queryset

    def list(self, request):
        serializer = EmailReadSerializer(self.get_queryset(), many=True,
                                     context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = EmailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(Email, pk=pk)
        serializer = EmailReadSerializer(instance, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        instance = get_object_or_404(Email, pk=pk)
        serializer = EmailSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        instance = get_object_or_404(Email, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class FBPageViewSet(viewsets.ViewSet):
    """
    A viewset that provides the standard actions
    """
    permission_classes = (IsAuthenticated,)
    queryset = FBPage.objects.all()
    serializer_class = FBPageSerializer

    def get_queryset(self):
        self.companies = Track.objects.filter(
            user=self.request.user).distinct().values_list('company')
        queryset = FBPage.objects.filter(company__in=self.companies)
        return queryset

    def list(self, request):
        serializer = FBPageReadSerializer(self.get_queryset(), many=True,
                                      context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = FBPageSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(FBPage, pk=pk)
        serializer = FBPageReadSerializer(instance, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        instance = get_object_or_404(FBPage, pk=pk)
        serializer = FBPageSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        instance = get_object_or_404(FBPage, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class PageViewSet(viewsets.ViewSet):
    """
    A viewset that provides the standard actions
    """
    permission_classes = (IsAuthenticated,)
    queryset = Page.objects.all()
    serializer_class = PageSerializer

    def get_queryset(self):
        self.companies = Track.objects.filter(
            user=self.request.user).distinct().values_list('company')
        queryset = Page.objects.filter(company__in=self.companies)
        return queryset

    def list(self, request):
        serializer = PageReadSerializer(self.get_queryset(), many=True,
                                    context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = PageSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(Page, pk=pk)
        serializer = PageReadSerializer(instance, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        instance = get_object_or_404(Page, pk=pk)
        serializer = PageSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        instance = get_object_or_404(Page, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TrackViewSet(viewsets.ViewSet):
    """
    A viewset that provides the standard actions
    """
    permission_classes = (IsAuthenticated,)
    queryset = Track.objects.all()
    serializer_class = TrackSerializer

    def get_queryset(self):

        queryset = Track.objects.filter(user_id=self.request.user)
        return queryset

    def list(self, request):
        queryset = Track.objects.all()
        serializer = TrackReadSerializer(queryset, many=True,
                                     context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = TrackSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(Track, pk=pk)
        serializer = TrackReadSerializer(instance, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        instance = get_object_or_404(Track, pk=pk)
        serializer = TrackSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        instance = get_object_or_404(Track, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class MainPageView(APIView):
    """
    Returns the Details of the Main Page
    """

    
    def get(self, request, format=None, **kwargs):
        emails_serializer = EmailReadSerializer(EmailViewSet.get_queryset(self), many=True,
                                            context={'request': request})
        fb_page_serializer = FBPageReadSerializer(FBPageViewSet.get_queryset(self), many=True,
                                              context={'request': request})
        page_serializer =  PageReadSerializer(PageViewSet.get_queryset(self), many=True,
                                         context={'request': request})
        tracked_company_serializer = TrackReadSerializer(TrackViewSet.get_queryset(self), many=True,
                                         context={'request': request})
        
        return Response({
            'emails': emails_serializer.data,
            'fb_page': fb_page_serializer.data,
            'page': page_serializer.data,
            'tracked_company':tracked_company_serializer.data
        })

