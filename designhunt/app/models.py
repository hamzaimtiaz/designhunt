# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.db.models import Exists, OuterRef

# fs = FileSystemStorage(location='/media/photos')
# Create your models here.


class Company(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    name = models.CharField(max_length=100)
    caption = models.CharField(max_length=100)
    website = models.CharField(max_length=100)
    image = models.ImageField(upload_to='company/')
    summary = models.TextField()
    address = models.TextField()
    twitter_followers = models.IntegerField(default=0)
    instagram_followers = models.IntegerField(default=0)

    class Meta:
        ordering = ['creation_time']

    def __str__(self):
        return self.name


class Email(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='email')
    screenshot = models.ImageField(upload_to='emails/')
    emails_tracked = models.IntegerField(default=0)

    class Meta:
        ordering = ['creation_time']


class FBPage(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='fbpage')
    screenshot = models.ImageField(upload_to='fbpage/')
    fbpages_tracked = models.IntegerField(default=0)

    class Meta:
        ordering = ['creation_time']


class Page(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='page')
    screenshot = models.ImageField(upload_to='page/')
    name = models.TextField()
    pages_tracked = models.IntegerField(default=0)

    class Meta:
        ordering = ['creation_time']


class Track(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='company_tracked')
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='user_tracked')

    class Meta:
        ordering = ['creation_time']
